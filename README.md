# pwcXTrakr

Our Submission for T-HUB Hackathon for PWC Category, A Expense Tracking App.

### Key features

- Expenses (add, edit, delete);
- Expense categories (add, edit, delete);
- Expense reports (filter by: today, week, month, date, date range);
- Currency chooser. 

### How to Run

 -  Build the project using Android Studio, with build tools > 25.
 -  Use the App on your Android Phone.